#pragma once
class Fat
{
protected://このクラスと継承したクラスで使える。
	char  name;//名前
	float height;//身長
	float weight;//体重
	
public:
	void Disp();
	void GetBMI();
};
