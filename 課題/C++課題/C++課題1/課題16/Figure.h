#include <iostream>
//基本となる図形クラス
class Figure
{
protected:
	float teihen, takasa, menseki;
public:
	void SetTeihen(float f);//底辺
	void SetTakasa(float f);//高さ
	void Disp();//計算して表示
};