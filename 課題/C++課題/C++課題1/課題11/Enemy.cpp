#include "Enemy.h"
#include <iostream>

Enemy::Enemy()
{
	hp = 300; //HP
	atk = 50; //攻撃力
	def = 35; //防御力
}

void Enemy::DispHp()
{
	std::cout << "プレイヤーＨＰ＝" << hp << "\n";
}

int Enemy::Attack(int i)
{
	printf("プレイヤーの攻撃！ ");//printf(文字を表示)
	return atk - i / 2;
}

void Enemy::Damage(int i)
{

	std::cout << "プレイヤーは" << i << "のダメージ\n";
	hp -= i;
}

int Enemy::GetDef()
{
	return def;
}

int Enemy::dispHp()
{
	return 0;
}

bool Enemy::IsDead()
{
	//HP が 0 以下だったら true を返す
	if (hp <= 0)
		return true;
	//それ以外なら false を返す
	return false;
}