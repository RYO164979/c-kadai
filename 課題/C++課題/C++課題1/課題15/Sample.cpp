#include <iostream>
#include "Sample.h" //クラスを宣言しているヘッダ
//メンバ変数に値を代入するメンバ関数
void SampleClass::Input() {
	a = 10;//10をaに代入
	b = 3;//3をaに代入
}
//メンバ変数同士の計算を行うメンバ関数
void SampleClass::Plus() {
	c = a + b;//a+bの結果を代入
}
//メンバ変数の内容を出力するメンバ関数
void SampleClass::Disp() {
	std::cout << "変数Ｃの値は" << c << "\n";
}