#include "Status.h"
//レベル値を受け取る（アクセス関数）
void Status::SetLv(int i)
{
	lv = i;//入力されたレベルを受け取る
}
//各パラメータを計算
void Status::Calc()
{
	hp = lv * lv + 50;//レベル×レベル+50
	atk = lv * 10;//レベル×10
	def = lv * 9;//レベル×9
}
//HP 値を返す（アクセス関数）
int Status::GetHp()
{
	return hp;//計算したHPの合計を返す。
}
//攻撃力値を返す（アクセス関数）
int Status::GetAtk()
{
	return atk;//計算した攻撃の合計を返す。
}
//防御力値を返す（アクセス関数）
int Status::GetDef()
{
	return def;//計算した防御の合計を返す。
}
