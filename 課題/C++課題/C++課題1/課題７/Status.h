class Status
{
	//メンバ変数
	int lv, hp, atk, def; //各パラメータ
	//メンバ関数

public://外から編集可能
	void SetLv(int i);
	void Calc();
	int GetHp();//体力
	int GetAtk();//攻撃力
	int GetDef();//防御力
};