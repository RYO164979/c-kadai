#include <iostream>
#include "Status.h"
int main()
{
	int lv;
	Status st; //クラスのインスタンス
	std::cout << "レベルを入力⇒";
	std::cin >> lv;//Lvに入力
	st.SetLv(lv); //レベル値を渡す
	st.Calc(); //各パラメータを設定
	//各パラメータを表示
	std::cout << " ＨＰ = " << st.GetHp() << "\n";//HPを表示
	std::cout << "攻撃力= " << st.GetAtk() << "\n";//攻撃力を表示
	std::cout << "防御力= " << st.GetDef() << "\n";//防御力を表示
}